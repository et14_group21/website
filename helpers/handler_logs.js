var database = require('../helpers/database');
var io = require('../helpers/io');
var dateFormat = require('dateformat');
var messages = require('../helpers/handler_messages');

/*
* formates the date in the format used in the log table
*/
formatDate = function(date){
  return dateFormat(date, 'dd.mm.yyyy HH:MM');
}

/*
* creates a JSON log object to be displayed in the log table
*/
createLog = function(data){
  data.timestamp = formatDate(data.timestamp);
  return data;
}

/*
* creates an array of log objects
*/
createProtocol = function(data){
	var logs = [];
  if(data){
    data.forEach(function(item){
  		logs.push(createLog(item));
  	});
  }
	return logs;
}

handleAllLogs = function(allLogs, callback){
  function getData(err, data){
    if(err || !data){
			messages.sent_message('Keine Protokolle abrufbar');
      return
		}
    else if(data.length == 0)
    {
      messages.sent_message('Keine Protokolle abrufbar');
      return;
    }
    callback(createProtocol(data));
  }
  if(allLogs){
    database.getAllLogs(getData);
  }
  else {
    database.getLogs(getData);
  }
}

module.exports.handleAllLogs = handleAllLogs;
