fs = require('fs');
const state_file1 = '../config/state_s1.txt';
const state_file2 = '../config/state_s2.txt';

/*
* reads the state file into a JSON object
*/
module.exports.readState = function(){
  try{
    var state1 = fs.readFileSync(state_file1, 'utf8');
    var state2 = fs.readFileSync(state_file2,'utf8');
  }
  catch(e){}

  return {
    s1: Boolean(state1==1),
    s2: Boolean(state2==1)
  };
}
