var mysql   = require('mysql');
var pool    = mysql.createPool({
  connectionLimit : 10,
  host            : 'localhost',
  user            : 'timerUser',
  password        : 'group21'
});

/*
* function to execute a sql query and get back data to the callback
*/
function sqlQuery(connectionString, callback)
{
  pool.query(connectionString, function(err, rows, fields) {
  	if (err)
  		callback(err, null);
  	else
  		callback(null, rows);
	});
}

exports.getEditConflict = function getEditConflict(date, callback){
  sqlQuery("SELECT * FROM timer.dates WHERE consumer = " + date.consumer + " AND (ID <> " + date.id + ") AND ((start >= '" + date.start + "' AND start <= '" + date.end + "') OR (start <= '" + date.start + "' AND end >= '" + date.start + "'))", function(err, data){
    if(!data){
			callback(false);
		}
		else if(data.length > 0){
      callback(true);
    }
    else {
      callback(false);
    }
  });
}

exports.getConflict = function getConflict(date, callback){
  sqlQuery("SELECT * FROM timer.dates WHERE consumer = " + date.consumer + " AND ((start >= '" + date.start + "' AND start <= '" + date.end + "') OR (start <= '" + date.start + "' AND end >= '" + date.start + "'))", function(err, data){
    if(!data){
			callback(false);
		}
		else if(data.length > 0){
      callback(true);
    }
    else {
      callback(false);
    }
  });
}

exports.getAllLogs = function getAllLogs(callback){
  sqlQuery('select * FROM timer.error', callback);
}

exports.getLogs = function getLogs(callback){
  sqlQuery('select * FROM timer.error where type = 1', callback);
}

exports.getConsumerSwitch = function getConsumerSwitch(callback){
  sqlQuery('select * FROM timer.error where code=0', callback);
}

exports.getDates = function getDates(callback){
  sqlQuery('select * FROM timer.dates', callback);
}

exports.getDateByID = function getDateByID(id, callback){
  sqlQuery('select * FROM timer.dates where ID = ' + pool.escape(id), callback);
}

exports.updateDateByID = function updateDateByID(date, callback){
  sqlQuery("update `timer`.`dates` SET `start`="  + pool.escape(date.start)
                                                      + ",`end`=" + pool.escape(date.end)
                                                      + ",`consumer`='" + pool.escape(date.consumer)
                                                      + "',`comment`=" + pool.escape(date.comment)
                                                      + ",`type`='" + pool.escape(date.type)
                                                      + "',`repeat`='" + pool.escape(date.repeat)
                                                      + "' where `ID`=" + pool.escape(date.id), callback);
}

exports.createNewDate = function createNewDate(date, callback){
  sqlQuery("insert into `timer`.`dates` (`start`, `end`, `consumer`, `comment`, `type`, `repeat`) Values ("  + pool.escape(date.start)
                                                      + "," + pool.escape(date.end)
                                                      + ",'" + pool.escape(date.consumer)
                                                      + "'," + pool.escape(date.comment)
                                                      + ",'" + pool.escape(date.type)
                                                      + "','" + pool.escape(date.repeat)
                                                      + "')", callback);
}

exports.deleteDate = function updateDateByID(id, callback){
  sqlQuery("DELETE FROM `timer`.`dates` WHERE `ID`=" + pool.escape(id), callback);
}
