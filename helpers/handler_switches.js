var config = require('../helpers/configuration')

module.exports = function(io){
  /*
  * function that gets called when a switch_settings event message is emitted
  */
  this.handleSwitchSettings = function(msg){
    config.updateSwitches(msg);
  }

  return this;
}
