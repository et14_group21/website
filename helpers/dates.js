var dateFormat = require('dateformat');

/*
* formates the date to the german format
* same as used by the datetimepicker
*/
module.exports.getDatePicker = function getDatePicker(date){
  return dateFormat(date, 'dd.mm.yyyy HH:MM');
}

/*
* formates the date to the american format
* same as used by the mysql database
*/
module.exports.getDateDatabase = function getDateDatabase(date){
  return dateFormat(date, 'yyyy-mm-dd HH:MM:ss');
}

/*
* parses a date in german format
* same as used by the datetimepicker
*/
module.exports.parseDate = function parseDate(string){

}
