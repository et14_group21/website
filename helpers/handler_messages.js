var io = require('../helpers/io');

exports.sent_message = function(message){
	io.sockets.emit('display_message', message);
}
