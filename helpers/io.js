var io = require('socket.io')();

var database_handler = require('../helpers/handler_database')(io);
var switches_handler = require('../helpers/handler_switches')(io);
var settings_handler = require('../helpers/handler_settings')(io);
var logs_handler = require('../helpers/handler_logs');

/*
* Common message to be displayed below navbar
*/
function print_message(message){
	io.emit('alert_message', {
		content: message.content,
		type: message.type
	});
}

/*
* Event occurring on client connection
*/
io.on('connection', function (socket) {
	console.log('Someone connected');

	//message event for displaying a message on the client
	socket.on('message', function(msg){
		console.log(JSON.stringify(msg));
		//TODO: Log this messages?
		print_message(msg);
	});

	//date_edit event for updating a date in the database
	socket.on('date_edit', function(msg){
		database_handler.handleDateEdit(msg);
	});

	//date_new event for adding a new date in the database
	socket.on('date_new', function(msg){
		database_handler.handleDateNew(msg);
	});

	//date_delete event for deleting a date in the database
	socket.on('date_delete', function(id){
		database_handler.handleDateDelete(id);
	});

	//switch_settings event for setting the useres settings of the switches
	socket.on('switch_settings', function(msg){
		switches_handler.handleSwitchSettings(msg);
	});

	//general_settings event for setting the useres settings
	socket.on('general_settings', function(msg){
		settings_handler.handleGeneralSettings(msg);
	});

	//show_all_logs event for displaying all logs
	socket.on('show_all_logs', function(msg){
		logs_handler.handleAllLogs(msg, function(data){
			io.emit('new_logs_message', data);
		});
	});
});

module.exports = io;
