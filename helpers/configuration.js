fs = require('fs');
const config_file = '../config/config.json';
const switch_files = ['../config/switch1.json', '../config/switch2.json'];

/*
* reads the config file into a JSON object
*/
module.exports.readConfig = function(){
  try{
    var config = fs.readFileSync(config_file, 'utf8');
  }
  catch(e){}
  if(config) {
    return JSON.parse(config);
  }
  else {
    return {
      speech_out: false,
      remote_control: false
    };
  }
}

/*
* reads the switch settings from file
*/
module.exports.readSwitchConfig = function(){
  var switches = [];
  try{
    switch_files.forEach(function(switchFile){
      var switchConf = fs.readFileSync(switchFile, 'utf8');
      switches.push(switchConf);
    });
  }
  catch(e){}
  if(switches.length >= 2){
    var s1 = JSON.parse(switches[0]);
    var s2 = JSON.parse(switches[1]);
    return {
      Switch1: s1.status,
      time1: s1.time,
      Switch2: s2.status,
      time2: s2.time
    };
  }
  else {
    return {
      Switch1: 0,
      time1: 0,
      Switch2: 0,
      time2: 0
    };
  }
}

/*
* writes the given JSON object to the config file
*/
module.exports.writeConfig = function(config){
  fs.writeFile(config_file, JSON.stringify(config, null, 4), function(err, data){
    if(err)
      console.log(err);
  });
}

/*
* write the JSON object for the swicht settings to file
*/
writeSwitchConfig = function(number, config){
  fs.writeFile(switch_files[number - 1], JSON.stringify(config, null, 4), function(err, data){
    if(err)
      console.log(err);
  });
}

/*
* updates the switch config in the config file
*/
module.exports.updateSwitches = function(switch_cfg){
  writeSwitchConfig(1, {
    status: switch_cfg.Switch1,
    time: switch_cfg.time1
  });
  writeSwitchConfig(2, {
    status: switch_cfg.Switch2,
    time: switch_cfg.time2,
  });

}

/*
* updates the settings config in the config file
*/
module.exports.updateSettings = function(settings_cfg){
  this.writeConfig(settings_cfg);
}
