module.exports = function(io){
  var database = require('../helpers/database');
  var dates = require('../helpers/dates');

  const SUCCESS = 'Termin erfolgreich in Datenbank geschrieben';
  const ERROR = 'Es ist ein Fehler aufgetreten';
  const OVERLAP = 'Termin nicht geschrieben!\nEs gibt eine Überschneidung mit einem bestehenden Termin';

  /*
  * prints the result message to the user
  */
  this.printDatabaseResult = function(message){
    io.emit('database_message', message);
  }

  /*
  * creates a Date JSON object for writing to the database
  * converts the data format of the datetime picker
  */
  this.createDate = function(obj){
    obj.start = dates.getDateDatabase(obj.start);
    obj.end = dates.getDateDatabase(obj.end);
    return obj;
  }

  /*
  * function that gets called when a date_edit event message is emitted
  */
  this.handleDateEdit = function(msg){
    database.getEditConflict(createDate(msg), function(conflict){
      if(!conflict){
        database.updateDateByID(createDate(msg), function(err, res){
          if(err){
            console.log(err);
            printDatabaseResult(ERROR);
          }
          else{
            console.log(JSON.stringify(res));
            printDatabaseResult(SUCCESS);
          }
        });
      }
      else {
        printDatabaseResult(OVERLAP);
        console.log("Conflict detected");
      }
    });
  }

  /*
  * function that gets called when a date_new event message is emitted
  */
  this.handleDateNew = function(msg){
    database.getConflict(createDate(msg), function(conflict){
      if(!conflict){
        database.createNewDate(createDate(msg), function(err, res){
          if(err){
            console.log(err);
            printDatabaseResult(ERROR);
          }
          else{
            console.log(JSON.stringify(res));
            printDatabaseResult(SUCCESS);
          }
        });
      }
      else {
        printDatabaseResult(OVERLAP);
        console.log("Conflict detected");
      }
    });
  }

  /*
  * function that gets called when a date_delete event message is emmited
  */
  this.handleDateDelete = function(id){
    database.deleteDate(id, function(err, res){
      if(err){
        console.log(err);
      }
      else{
        console.log(JSON.stringify(res));
      }
    });
  }

  return this;
}
