var config = require('../helpers/configuration')

module.exports = function(io){
  /*
  * function that gets called when a general_settings event message is emitted
  */
  this.handleGeneralSettings = function(msg){
    config.updateSettings(msg);
  }

  return this;
}
