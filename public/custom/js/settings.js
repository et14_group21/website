/*
* collects the date data entered by the user
*/
function collectData(){
  return {
    speech_out: document.getElementById('cb_speech').checked,
    remote_control: document.getElementById('cb_remote').checked,
  };
}

/*
* display the save and cancle button
*/
function display_buttons(visible){
  if(visible){
    $("#dialog_button").removeClass('hidden');
  }
  else {
    $("#dialog_button").addClass('hidden');
  }
}

/*
* function triggered on save button click
*/
function saveClicked(){
  //alert(JSON.stringify(collectData()));
  socket.emit('general_settings', collectData());
  display_buttons(false);
}

/*
* function triggered on cancle button click
*/
function cancleClicked(){
  location.reload();
}

/*
* function to be called on checkbutton value change
*/
function change(){
  display_buttons(true);
}
