/*
* function to be called on checkbutton value change
*/
function change(box){
  socket.emit('show_all_logs', box.checked);
}

/*
* function to clear the table
*/
function clearTable(){
  // Find the table element
  var table = document.getElementById("LogTable");

  while(table.rows.length > 2) {
    table.deleteRow(table.rows.length - 1);
  }
}

/*
* function to update the log table
*/
function updateTable(logs){
  clearTable();
  // Find the table element
  var table = document.getElementById("LogTable");

  logs.forEach(function(log){
    // Create an empty <tr> element and add it to the end of the table:
    var row = table.insertRow(table.rows.length);

    // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);

    // Add some text to the new cells:
    cell1.innerHTML = log.ID;
    cell2.innerHTML = log.timestamp;
    cell3.innerHTML = log.type;
    cell4.innerHTML = log.code;
    cell5.innerHTML = log.name;
  });
}

/*
* function to display new log entries
*/
socket.on('new_logs_message', function(msg){
  //alert(JSON.stringify(msg));
  updateTable(msg);
});
