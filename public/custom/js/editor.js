/*
* setup the first datetimepicker
*/
$(function () {
  $('#datetimepicker1').datetimepicker({
      locale: 'de'
    });
});

/*
* setup the second datetimepicker
*/
$(function () {
  $('#datetimepicker2').datetimepicker({
      locale: 'de'
    });
});

/*
* function that checks if user input is valid
*/
function checkValid(){
  var data = collectData();
  if(!data.start || !data.end){
    alert('Gültiges Anfangs- und Enddatum angeben');
    return false;
  }
  if(data.end <= data.start){
    alert('Anfangspunkt muss vor Endpunkt liegen');
    return false;
  }
  if(!data.comment.trim()){
    alert('Bitte Beschreibung eingeben');
    return false;
  }
  return true;
}

/*
* function triggered on index of repeat change
*/
function index_changed(dropdown){
  if(dropdown.selectedIndex != 0){
    alert('Bei sich wiederholenden Terminen\nwird immer nur der nächste Termin im Kalender angezeigt');
  }
}

/*
* function triggered on cancle button click
*/
function cancleClicked(){
  var cancle = confirm('Bearbeiten wirklich beenden?\nAlle ungespeicherten Änderungen gehen verloren!');
  if(cancle)
    window.location.replace('/calendar');
}

/*
* function to display database messages
*/
socket.on('database_message', function(msg){
  alert(msg);
});
