//Test function for emit event
function something() {
  socket.emit('message', {
    content: 'Turn all the lights on!',
    type: 'alert-info'
  });
}

/*
* collects the date data entered by the user
*/
function collectData(){
  return {
    Switch1: document.getElementById('switch1').selectedIndex,
    time1: Number(document.getElementById('timertime1').value),
    Switch2: document.getElementById('switch2').selectedIndex,
    time2: Number(document.getElementById('timertime2').value)
  };
}

/*
* display the save and cancle button
*/
function display_buttons(visible){
  if(visible){
    $("#dialog_button").removeClass('hidden');
  }
  else {
    $("#dialog_button").addClass('hidden');
  }
}

/*
* function triggered on save button click
*/
function saveClicked(){
  //alert(JSON.stringify(collectData()));
  socket.emit('switch_settings', collectData());
  display_buttons(false);
}

/*
* function triggered on cancle button click
*/
function cancleClicked(){
  location.reload();
}


/*
* show the time input element for timer duration
*/
function show_time_input(timer_number) {
  document.getElementById(timer_number+'_timer').className ='input-group col-md-6';
}

/*
* hide the time input element
*/
function hide_time_input(timer_number) {
  document.getElementById(timer_number+'_timer').className += ' hidden';
}

/*
* function to be called on dropdown index change
*/
function index_changed(dropdown_number) {
  display_buttons(true);
  var selected_option = document.getElementById('switch'+dropdown_number).selectedIndex;
  if (selected_option == 3){
    show_time_input(dropdown_number);
  }
  else{
    hide_time_input(dropdown_number);
  }
}
