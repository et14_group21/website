/*
* collects the date data entered by the user
*/
function collectData(){
  return {
    id : getID(),
    start : $('#datetimepicker1').data("DateTimePicker").date(),
    end : $('#datetimepicker2').data("DateTimePicker").date(),
    consumer : (document.getElementById('d_type').selectedIndex == 2?0:document.getElementById('d_type').selectedIndex+1),
    comment : document.getElementById('d_name').value,
    type : (document.getElementById('d_type').selectedIndex == 2?2:1),
    repeat: document.getElementById('d_repeat').selectedIndex
  };
}

/*
* function triggered on delete button click
*/
function deleteClicked(){
  var del_confirm = confirm('Termin wirklich löschen?');
  if(del_confirm){
    socket.emit('date_delete', getID());
  }
}

/*
* function triggered on save button click
*/
function saveClicked(){
  //alert(JSON.stringify(collectData()));
  if(checkValid())
    socket.emit('date_edit', collectData());
}
