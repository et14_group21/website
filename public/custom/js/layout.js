// initialize socket.io client script
var socket = io();

/*
* display a message under the navbar
* @message: Message text string
* @type: alert type {alert-info, alert-success, alert-danger}
*/
function addAlert(content, type) {
  if (!type){
    type = "alert-info";
  }
  $('#alert_message').append(
    '<div class="alert alert-dismissible ' + type + '" role="alert">' +
      '<button type="button" class="close" data-dismiss="alert">' + '&times;</button>' +
      content +
    '</div>');
}

/*
* display a message on the page
*/
function displayMessage(message) {
  alert(message);
}

/*
* event on alert_message sent by server
*/
socket.on('alert_message', function(msg){
  addAlert(msg.content, msg.type);
});

/*
* event on display_message sent by server
*/
socket.on('display_message', function(msg){
  displayMessage(msg);
});
