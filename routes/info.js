var express = require('express');
var router = express.Router();

var app_version = require('../helpers/version');

/* GET info page. */
router.get('/', function(req, res, next) {
  res.render('info', {
    title: 'Informationen',
    version: app_version 
  });
});

module.exports = router;
