var express = require('express');
var router = express.Router();

var config = require('../helpers/configuration')
var state = require('../helpers/state')

/* GET switches page. */
router.get('/', function(req, res, next) {
  res.render('switches', {
    title: 'Tasterverwaltung',
    config: config.readSwitchConfig(),
    switches: state.readState()
  });
});

module.exports = router;
