var express = require('express');
var router = express.Router();

var database = require('../helpers/database');

const VACATION_MODE = 2;

/*
* converts a date from the database to a date, that the bootstrap-calendar uses
*/
function convertDate(date){
  return {
    "id": date.ID,
    "title": date.comment + (date.type == VACATION_MODE ? '' : ' (Verbraucher ' + date.consumer + ')'),
    "url":   '/editor?id=' + date.ID,
    "class": (date.type == VACATION_MODE ? 'event-special' : 'event-success'),
    "start": date.start.getTime(),
    "end":   date.end.getTime()
  };
}

/*
* creates a date array to be used by the bootstrap-calendar
*/
function createArray(data){
  var dates = [];
  data.forEach(function(date){
    dates.push(convertDate(date));
  });
  return dates;
}

/*
* GET calendar page.
*/
router.get('/', function(req, res, next) {
  database.getDates(function(err, data){
    if(err || !data){
      console.log('There was an error');
			console.log(err);
    }
    else {
      var dates = createArray(data);
    }

    /*
    * render the webpage with the date array created
    **/
    res.render('calendar', {
      title: 'Kalender',
      dates: dates
    });
  })
});

module.exports = router;
