var express = require('express');
var router = express.Router();

var config = require('../helpers/configuration')

/* GET settings page. */
router.get('/', function(req, res, next) {
  res.render('settings', {
    title: 'Einstellungen',
    config: config.readConfig()
  });
});

module.exports = router;
