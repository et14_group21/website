var express = require('express');
var router = express.Router();

var database = require('../helpers/database');
var messages = require('../helpers/handler_messages');
var dateFormat = require('dateformat');

/*
* formates the date in the format used in the log table
*/
function formatDate(date){
  return dateFormat(date, 'dd.mm.yyyy HH:MM');
}

/*
* creates a JSON log object to be displayed in the log table
*/
function createLog(data){
  data.timestamp = formatDate(data.timestamp);
  return data;
}

/*
* creates an array of log objects
*/
function createProtocol(data){
	var logs = [];
  if(data){
    data.forEach(function(item){
  		logs.push(createLog(item));
  	});
  }
	return logs;
}


/* GET protocol page. */
router.get('/', function(req, res, next) {
  database.getLogs(function(err, data){
    var success = true;
		if(err || !data){
			messages.sent_message('Keine Protokolle abrufbar');
      success = false;
		}
    else if(data.length == 0)
    {
      messages.sent_message('Keine Protokolle vorhanden');
      success = false;
    }

		console.log(JSON.stringify(data));
		//console.log(JSON.stringify(createProtocol(data)));

		res.render('logs', {
				title: 'Protokoll',
				logs: createProtocol(data),
        protsuccess: success
		});
	});
});

module.exports = router;
