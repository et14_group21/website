var express = require('express');
var router = express.Router();

var database = require('../helpers/database');
var dates= require('../helpers/dates');

/*
* creates a JSON object to be displayed in the editor
*/
function createDate(data){
  data.start = dates.getDatePicker(data.start);
  data.end = dates.getDatePicker(data.end);
  return data;
}

function renderNew(res){
  res.render('editor_new');
}

/* GET editor page. */
router.get('/', function(req, res){
  console.log(req.query.id);
  if(req.query.id) {
    database.getDateByID(req.query.id, function(err, data){
      if(err){
        console.log('an error happened');
        renderNew(res);
        return;
      }
      else if(!data || data.length <= 0){
        console.log("No date found.")
        renderNew(res);
        return;
      }
      //console.log(JSON.stringify(data));
      //console.log(JSON.stringify(createDate(data[0])));
      res.render('editor_edit', {
        date_ID: req.query.id,
        date: createDate(data[0]),
      });
    });
  }
  else{
    renderNew(res);
  }
});

module.exports = router;
